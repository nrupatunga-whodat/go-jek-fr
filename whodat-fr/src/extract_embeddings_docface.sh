#!/bin/sh

ROOT_DIR="/home/nrupatunga/2019/whodat/go-jek-fr"
IN_DIR="$ROOT_DIR/whodat-fr/data/lfw_dummy/"
OUT_DIR="$ROOT_DIR/whodat-fr/data/lfw_dummy_align/"

# face alignment
python face_align_w.py $IN_DIR $OUT_DIR --image_size 112 --gpu_memory_fraction 0.7

# extract feature from DocFace base model
find $OUT_DIR -name "*.png" -o -name "*.jpg" > image_list.txt
DOC_FACE_MODEL_DIR="$ROOT_DIR/DocFace/src/faceres_ms"
IMAGE_LIST='./image_list.txt'
OUTPUT_NUMPY_FILE='embeddings_docface.npy'
BATCH_SIZE=128

python ./extract_embeddings_w.py --model_dir=$DOC_FACE_MODEL_DIR --image_list=$IMAGE_LIST --output=$OUTPUT_NUMPY_FILE --batch_size=$BATCH_SIZE
echo 'Done'
