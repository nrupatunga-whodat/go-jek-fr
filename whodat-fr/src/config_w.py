# ==============================================================================
# Copyright (c) 2019 Whodat Tech Pvt Ltd.
# All Rights Reserved.
# Whodat Confidential and Proprietary
# ==============================================================================

"""
# File: global_config.py
# Author: Nrupatunga
# Email: nrupatunga.tunga@gmail.com
# Github: https://github.com/nrupatunga
# Description: This file consists of all the global config needed for the
# project
"""

import os
import sys

PROJECT_DIR = os.path.dirname(os.path.dirname(os.getcwd()))

facenet_dirs = ['facenet',
                'facenet/contributed',
                'facenet/src',
                'facenet/src/align',
                'facenet/src/generative']
docface_dirs = ['DocFace',
                'DocFace/config',
                'DocFace/nets',
                'DocFace/src',
                ]

insightface_dirs = ['insightface',
                    'insightface/deploy']

# Adding the facenet and docface code paths to the sys paths
for sub_dir in facenet_dirs:
    sys.path.insert(0, os.path.join(PROJECT_DIR, sub_dir))

for sub_dir in docface_dirs:
    sys.path.insert(0, os.path.join(PROJECT_DIR, sub_dir))

# Adding the arcface code paths to the sys paths
for sub_dir in insightface_dirs:
    sys.path.insert(0, os.path.join(PROJECT_DIR, sub_dir))
