# ==============================================================================
# Copyright (c) 2019 Whodat Tech Pvt Ltd.
# All Rights Reserved.
# Whodat Confidential and Proprietary
# ==============================================================================

"""
File: extract_embeddings_w.py
Author: Nrupatunga
Email: nrupatunga.tunga@gmail.com
Github: https://github.com/nrupatunga
Description: extract the embeddings from the network (docface currently used)
"""

import config_w as cfr  # noqa
import extract_features as docface_base_model
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_dir", help="The path to the pre-trained model directory",
                        type=str, required=True)
    parser.add_argument("--image_list", help="The list file to aligned face images to extract features",
                        type=str, required=True)
    parser.add_argument("--output", help="The output numpy file to store the extracted features",
                        type=str, required=True)
    parser.add_argument("--batch_size", help="Number of images per mini batch",
                        type=int, default=128)
    args = parser.parse_args()
    docface_base_model.main(args)
