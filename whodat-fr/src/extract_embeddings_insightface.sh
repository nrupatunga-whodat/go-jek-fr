#!/bin/sh

ROOT_DIR="/home/nrupatunga/2019/whodat/go-jek-fr"
IN_DIR="$ROOT_DIR/whodat-fr/data/lfw_dummy/"
OUT_DIR="$ROOT_DIR/whodat-fr/data/lfw_dummy_align/"

# face alignment
python face_align_w.py $IN_DIR $OUT_DIR --image_size 112 --gpu_memory_fraction 0.7

# extract feature from DocFace base model
find $OUT_DIR -name "*.png" -o -name "*.jpg" > image_list.txt

INSIGHTFACE_MODEL_DIR="$ROOT_DIR/insightface/models/model" # follow this convention to specify the model path for insightface
echo $INSIGHTFACE_MODEL_DIR
IMAGE_LIST='./image_list.txt'
OUTPUT_NUMPY_FILE='embeddings_insightface.npy'

python ./extract_embeddings_insightface_w.py --image_list=$IMAGE_LIST --model="$INSIGHTFACE_MODEL_DIR, 0" --output=$OUTPUT_NUMPY_FILE
echo 'Done'
