# ==============================================================================
# Copyright (c) 2019 Whodat Tech Pvt Ltd.
# All Rights Reserved.
# Whodat Confidential and Proprietary
# ==============================================================================

"""
File: extract_embeddings_insightface.py
Author: Nrupatunga
Email: nrupatunga.tunga@gmail.com
Github: https://github.com/nrupatunga
Description: extract the embeddings from the network (arcface is used)
"""

import config_w as cfr  # noqa
import face_model
import argparse
import cv2
import numpy as np


def main(args):
    """Extract the arcface features for all the images

    :args: TODO
    :returns: TODO

    """
    model = face_model.FaceModel(args)
    features = []
    with open(args.image_list) as f:
        for line in f:
            img_file = line.strip().split()[0]
            print('Extracting feature for {}'.format(img_file))
            img = cv2.imread(img_file)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = np.transpose(img, (2, 0, 1))
            feature = model.get_feature(img)
            features.append(feature)

    features_insightface = np.array(features)
    np.save(args.output, features_insightface)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Arcface feature extraction')
    # general
    parser.add_argument("--image_list", help="The list file to aligned face images to extract features", type=str, required=True)
    parser.add_argument('--image-size', default='112,112', help='')
    parser.add_argument('--model', default='', help='path to load model.')
    parser.add_argument('--ga-model', default='', help='path to load model.')
    parser.add_argument('--gpu', default=0, type=int, help='gpu id')
    parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
    parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
    parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
    parser.add_argument("--output", help="The output numpy file to store the extracted features",
                        type=str, required=True)
    args = parser.parse_args()
    main(args)
