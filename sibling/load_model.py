import mxnet as mx
import fresnet
import argparse
import math
import numpy as np
import pdb
from image_iter import FaceImagePairIter
import mxnet.optimizer as optimizer
import os
import face_image
import logging
from extract_features_sibling import extract_features
from easydict import EasyDict as edict

logger = logging.getLogger()
logger.setLevel(logging.INFO)

class AccMetric(mx.metric.EvalMetric):
  def __init__(self, name):
    self.axis = 1
    super(AccMetric, self).__init__(
        'acc', axis=self.axis,
        output_names=None, label_names=None)
    self.losses = []
    self.count = 0
    self.name = name
    if name == 'doc':
        self.pred_idx = 1
        self.label_idx = 0
    else:
        self.pred_idx = 4
        self.label_idx = 1

  def update(self, labels, preds):
    self.count+=1
    label = labels[self.label_idx]
    pred_label = preds[self.pred_idx]
    if pred_label.shape != label.shape:
        pred_label = mx.ndarray.argmax(pred_label, axis=self.axis)
    pred_label = pred_label.asnumpy().astype('int32').flatten()
    label = label.asnumpy()
    if label.ndim==2:
      label = label[:,0]
    label = label.astype('int32').flatten()
    assert label.shape==pred_label.shape
    self.sum_metric += (pred_label.flat == label.flat).sum()
    self.num_inst += len(pred_label.flat)

class LossValueMetric(mx.metric.EvalMetric):
  def __init__(self, name):
    self.axis = 1
    super(LossValueMetric, self).__init__(
        'lossvalue', axis=self.axis,
        output_names=None, label_names=None)
    self.losses = []
    self.name = name
    if name == 'doc':
        self.pred_idx = 2
    else:
        self.pred_idx = 5

  def update(self, labels, preds):
    loss = preds[self.pred_idx].asnumpy()[0]
    self.sum_metric += loss
    self.num_inst += 1.0
    # gt_label = preds[-2].asnumpy()
    #print(gt_label)


def get_symbol(args, arg_params, aux_params,_weight,input_name,output_name,shared_weights = dict()):
  data_shape = (args.image_channel,args.image_h,args.image_w)
  image_shape = ",".join([str(x) for x in data_shape])
  margin_symbols = []
  
  print('init resnet', args.num_layers)
  embedding = fresnet.get_symbol(args.emb_size, args.num_layers, input_name,shared_weights=shared_weights,
        version_se=args.version_se, version_input=args.version_input, 
        version_output=args.version_output, version_unit=args.version_unit,
        version_act=args.version_act)
  
  #all_label = mx.symbol.Variable('softmax_label')
  all_label = mx.symbol.Variable(output_name)
  gt_label = all_label
  #gt_label = label
  extra_loss = None
  #_weight = mx.symbol.Variable("fc7_weight", shape=(args.num_classes, args.emb_size), lr_mult=args.fc7_lr_mult, wd_mult=args.fc7_wd_mult)
  if args.loss_type==0: #softmax
    if args.fc7_no_bias:
      fc7 = mx.sym.FullyConnected(data=embedding, weight = _weight, no_bias = True, num_hidden=args.num_classes, name='fc7')
    else:
      _bias = mx.symbol.Variable('fc7_bias', lr_mult=2.0, wd_mult=0.0)
      fc7 = mx.sym.FullyConnected(data=embedding, weight = _weight, bias = _bias, num_hidden=args.num_classes, name='fc7')
  elif args.loss_type==1: #sphere
    _weight = mx.symbol.L2Normalization(_weight, mode='instance')
    fc7 = mx.sym.LSoftmax(data=embedding, label=gt_label, num_hidden=args.num_classes,
                          weight = _weight,
                          beta=args.beta, margin=args.margin, scale=args.scale,
                          beta_min=args.beta_min, verbose=1000, name='fc7')
  elif args.loss_type==2: # reduce FC7 output by prescribed
    s = args.margin_s 
    m = args.margin_m
    assert(s>0.0)
    assert(m>0.0)
    _weight = mx.symbol.L2Normalization(_weight, mode='instance')
    nembedding = mx.symbol.L2Normalization(embedding, mode='instance', name='fc1n')*s
    fc7 = mx.sym.FullyConnected(data=nembedding, weight = _weight, no_bias = True, num_hidden=args.num_classes, name='fc7')
    s_m = s*m
    gt_one_hot = mx.sym.one_hot(gt_label, depth = args.num_classes, on_value = s_m, off_value = 0.0)
    fc7 = fc7-gt_one_hot
  elif args.loss_type==4:
    s = args.margin_s
    m = args.margin_m
    assert s>0.0
    assert m>=0.0
    assert m<(math.pi/2)
    _weight = mx.symbol.L2Normalization(_weight, mode='instance')
    nembedding = mx.symbol.L2Normalization(embedding, mode='instance', name=input_name+'_fc1n')*s
    fc7 = mx.sym.FullyConnected(data=nembedding, weight = _weight, no_bias = True, num_hidden=args.num_classes, name=input_name+'_fc7')
    zy = mx.sym.pick(fc7, gt_label, axis=1)
    cos_t = zy/s
    cos_m = math.cos(m)
    sin_m = math.sin(m)
    mm = math.sin(math.pi-m)*m
    #threshold = 0.0
    threshold = math.cos(math.pi-m)
    if args.easy_margin:
      cond = mx.symbol.Activation(data=cos_t, act_type='relu')
    else:
      cond_v = cos_t - threshold
      cond = mx.symbol.Activation(data=cond_v, act_type='relu')
    body = cos_t*cos_t
    body = 1.0-body
    sin_t = mx.sym.sqrt(body)
    new_zy = cos_t*cos_m
    b = sin_t*sin_m
    new_zy = new_zy - b
    new_zy = new_zy*s
    if args.easy_margin:
      zy_keep = zy
    else:
      zy_keep = zy - s*mm
    new_zy = mx.sym.where(cond, new_zy, zy_keep)

    diff = new_zy - zy
    diff = mx.sym.expand_dims(diff, 1)
    gt_one_hot = mx.sym.one_hot(gt_label, depth = args.num_classes, on_value = 1.0, off_value = 0.0)
    body = mx.sym.broadcast_mul(gt_one_hot, diff)
    fc7 = fc7+body
  elif args.loss_type==5:
    s = args.margin_s
    m = args.margin_m
    assert s>0.0
    _weight = mx.symbol.L2Normalization(_weight, mode='instance')
    nembedding = mx.symbol.L2Normalization(embedding, mode='instance', name='fc1n')*s
    fc7 = mx.sym.FullyConnected(data=nembedding, weight = _weight, no_bias = True, num_hidden=args.num_classes, name='fc7')
    if args.margin_a!=1.0 or args.margin_m!=0.0 or args.margin_b!=0.0:
      if args.margin_a==1.0 and args.margin_m==0.0:
        s_m = s*args.margin_b
        gt_one_hot = mx.sym.one_hot(gt_label, depth = args.num_classes, on_value = s_m, off_value = 0.0)
        fc7 = fc7-gt_one_hot
      else:
        zy = mx.sym.pick(fc7, gt_label, axis=1)
        cos_t = zy/s
        t = mx.sym.arccos(cos_t)
        if args.margin_a!=1.0:
          t = t*args.margin_a
        if args.margin_m>0.0:
          t = t+args.margin_m
        body = mx.sym.cos(t)
        if args.margin_b>0.0:
          body = body - args.margin_b
        new_zy = body*s
        diff = new_zy - zy
        diff = mx.sym.expand_dims(diff, 1)
        gt_one_hot = mx.sym.one_hot(gt_label, depth = args.num_classes, on_value = 1.0, off_value = 0.0)
        body = mx.sym.broadcast_mul(gt_one_hot, diff)
        fc7 = fc7+body
  elif args.loss_type==6: #intra loss
    s = args.margin_s
    m = args.margin_m
    assert s>0.0
    assert args.margin_b>0.0
    _weight = mx.symbol.L2Normalization(_weight, mode='instance')
    nembedding = mx.symbol.L2Normalization(embedding, mode='instance', name='fc1n')*s
    fc7 = mx.sym.FullyConnected(data=nembedding, weight = _weight, no_bias = True, num_hidden=args.num_classes, name='fc7')
    zy = mx.sym.pick(fc7, gt_label, axis=1)
    cos_t = zy/s
    t = mx.sym.arccos(cos_t)
    intra_loss = t/np.pi
    intra_loss = mx.sym.mean(intra_loss)
    #intra_loss = mx.sym.exp(cos_t*-1.0)
    intra_loss = mx.sym.MakeLoss(intra_loss, name='intra_loss', grad_scale = args.margin_b)
    if m>0.0:
      t = t+m
      body = mx.sym.cos(t)
      new_zy = body*s
      diff = new_zy - zy
      diff = mx.sym.expand_dims(diff, 1)
      gt_one_hot = mx.sym.one_hot(gt_label, depth = args.num_classes, on_value = 1.0, off_value = 0.0)
      body = mx.sym.broadcast_mul(gt_one_hot, diff)
      fc7 = fc7+body
  elif args.loss_type==7: #inter loss
    s = args.margin_s
    m = args.margin_m
    assert s>0.0
    assert args.margin_b>0.0
    assert args.margin_a>0.0
    _weight = mx.symbol.L2Normalization(_weight, mode='instance')
    nembedding = mx.symbol.L2Normalization(embedding, mode='instance', name='fc1n')*s
    fc7 = mx.sym.FullyConnected(data=nembedding, weight = _weight, no_bias = True, num_hidden=args.num_classes, name='fc7')
    zy = mx.sym.pick(fc7, gt_label, axis=1)
    cos_t = zy/s
    t = mx.sym.arccos(cos_t)

    #counter_weight = mx.sym.take(_weight, gt_label, axis=1)
    #counter_cos = mx.sym.dot(counter_weight, _weight, transpose_a=True)
    counter_weight = mx.sym.take(_weight, gt_label, axis=0)
    counter_cos = mx.sym.dot(counter_weight, _weight, transpose_b=True)
    #counter_cos = mx.sym.minimum(counter_cos, 1.0)
    #counter_angle = mx.sym.arccos(counter_cos)
    #counter_angle = counter_angle * -1.0
    #counter_angle = counter_angle/np.pi #[0,1]
    #inter_loss = mx.sym.exp(counter_angle)

    #counter_cos = mx.sym.dot(_weight, _weight, transpose_b=True)
    #counter_cos = mx.sym.minimum(counter_cos, 1.0)
    #counter_angle = mx.sym.arccos(counter_cos)
    #counter_angle = mx.sym.sort(counter_angle, axis=1)
    #counter_angle = mx.sym.slice_axis(counter_angle, axis=1, begin=0,end=int(args.margin_a))

    #inter_loss = counter_angle*-1.0 # [-1,0]
    #inter_loss = inter_loss+1.0 # [0,1]
    inter_loss = counter_cos
    inter_loss = mx.sym.mean(inter_loss)
    inter_loss = mx.sym.MakeLoss(inter_loss, name='inter_loss', grad_scale = args.margin_b)
    if m>0.0:
      t = t+m
      body = mx.sym.cos(t)
      new_zy = body*s
      diff = new_zy - zy
      diff = mx.sym.expand_dims(diff, 1)
      gt_one_hot = mx.sym.one_hot(gt_label, depth = args.num_classes, on_value = 1.0, off_value = 0.0)
      body = mx.sym.broadcast_mul(gt_one_hot, diff)
      fc7 = fc7+body
  out_list = [mx.symbol.BlockGrad(embedding)]
  softmax = mx.symbol.SoftmaxOutput(data=fc7, label = gt_label, name='softmax'+'_'+input_name, normalization='valid')
  out_list.append(softmax)
  if args.loss_type==6:
    out_list.append(intra_loss)
  if args.loss_type==7:
    out_list.append(inter_loss)
    #out_list.append(mx.sym.BlockGrad(counter_weight))
    #out_list.append(intra_loss)
  if args.ce_loss:
    #ce_loss = mx.symbol.softmax_cross_entropy(data=fc7, label = gt_label, name='ce_loss')/args.per_batch_size
    body = mx.symbol.SoftmaxActivation(data=fc7)
    body = mx.symbol.log(body)
    _label = mx.sym.one_hot(gt_label, depth = args.num_classes, on_value = -1.0, off_value = 0.0)
    body = body*_label
    ce_loss = mx.symbol.sum(body)/args.per_batch_size
    out_list.append(mx.symbol.BlockGrad(ce_loss))
  out = mx.symbol.Group(out_list)
  return (out, arg_params, aux_params)

def main_net(args):
  vec = args.pretrained.split(',')
  args.rescale_threshold = 0
  args.image_channel = 3
  args.image_h = 112
  args.image_w = 112
  args.num_layers = int(args.network[1:])
  print('num_layers', args.num_layers)
  #args.num_classes = 20000

  os.environ['BETA'] = str(args.beta)
  data_dir_list = args.data_dir.split(',')
  assert len(data_dir_list)==1
  data_dir = data_dir_list[0]
  path_imgrec = None
  path_imglist = None
  prop = face_image.load_property(data_dir)
  args.num_classes = prop.num_classes


  print('loading', vec)
  _, arg_params, aux_params = mx.model.load_checkpoint(vec[0], int(vec[1]))
  weights = mx.symbol.Variable("fc7_weight", shape=(args.num_classes, args.emb_size), lr_mult=args.fc7_lr_mult, wd_mult=args.fc7_wd_mult)
  
  #create placeholders for doc and selfie input/label
  doc_data = mx.symbol.Variable("doc")
  selfie_data = mx.symbol.Variable("selfie")
  doc_label = mx.symbol.Variable("doc_label")
  selfie_label = mx.symbol.Variable("selfie_label")

  os.environ['BETA'] = str(args.beta)
  data_dir_list = args.data_dir.split(',')
  assert len(data_dir_list)==1
  data_dir = data_dir_list[0]
  path_imgrec = None
  path_imglist = None
  prop = face_image.load_property(data_dir)
  args.num_classes = prop.num_classes

  num_shared_stages = args.num_shared_stages

  pre_resnet_stage = ['conv0_weight','bn0_beta','bn0_gamma','relu0_gamma']
  post_conv_weights = ['bn1_beta','bn1_gamma','pre_fc1_weight','pre_fc1_bias','fc1_beta','fc1_gamma']

  shared_vars = []
  num_stages = 4

  #No stages shared
  if num_shared_stages <= -1:
    print 'Not sharing any params'
    shared_vars = []

  #share only FC1
  elif num_shared_stages == 0:
    print 'Sharing only FC1 params'
    for key in post_conv_weights:
      shared_vars.append(key)  
  
  else:
    #custom stage sharing based on user param
    for key in post_conv_weights:
      shared_vars.append(key)  
    
    num_shared_stages = min(num_shared_stages,num_stages)
    print('Sharing {} stages params'.format(num_shared_stages))
    for stage_id in range(4-num_shared_stages,num_stages):
      str1 = 'stage%d'%(stage_id+1)
      shared_stages.append(str1)  
    
    for key in sorted(arg_params):
      for shared_name in shared_stages:
        if shared_name in key:
          shared_vars.append(key)

  shared_weights = dict()

  #create variables for shared params
  for key in sorted(shared_vars):
    #print key
    key_variable = mx.symbol.Variable(key)
    shared_weights[key] = key_variable

  #create symbol for Doc and Selfie sibling network  
  sym1, arg_params1, aux_params1 = get_symbol(args, arg_params, aux_params,weights,"selfie","selfie_label",shared_weights)
  sym0, arg_params0, aux_params0 = get_symbol(args, arg_params, aux_params,weights,"doc","doc_label",shared_weights)
  
  #Create params for weight initialization from pretrained net
  all_args = dict()
  all_aux = dict()

  for key in sorted(arg_params):
    
    if key in shared_vars:
      all_args[key] = arg_params[key]  
    
    else:
      new_name = 'doc_' + key 
      all_args[new_name] = arg_params[key]

      new_name = 'selfie_' + key 
      all_args[new_name] = arg_params[key]

  for key in sorted(aux_params):
    new_name = 'doc_' + key 
    all_aux[new_name] = aux_params[key]

    new_name = 'selfie_' + key 
    all_aux[new_name] = aux_params[key]

  #group sibling net symbols for training
  out_sym = mx.sym.Group([sym0,sym1])

  #create model for sibling network
  model = mx.mod.Module(
    context       = mx.gpu(0),
    symbol        = out_sym,
    data_names    = ["doc","selfie"],
    label_names   = ["doc_label","selfie_label"]
  )
  
  #Create data generator for training sibling net

  train_dataiter = FaceImagePairIter(
        batch_size           = args.per_batch_size,
        data_shape           = (3,112,112),
        path_imgrec_doc      = args.data_dir + "doc/train.rec",
        path_imgrec_selfie   = args.data_dir + "selfie/train.rec",
        shuffle              = True,
        rand_mirror          = True,
        mean                 = None,
        cutoff               = False,
        color_jittering      = False,
        images_filter        = False,
  )
  
  #create metrics for doc and selfie
  metric_doc = AccMetric('doc')
  metric_selfie = AccMetric('selfie')
  eval_metrics = [mx.metric.create(metric_doc), mx.metric.create(metric_selfie)]
  if args.ce_loss:
    l_metric_doc = LossValueMetric('doc')
    l_metric_selfie = LossValueMetric('selfie')
    eval_metrics.append( mx.metric.create(l_metric_doc) )
    eval_metrics.append( mx.metric.create(l_metric_selfie) )

  #create optimizer 
  begin_epoch = 0
  base_lr = args.lr
  base_wd = args.wd
  base_mom = args.mom
  opt = optimizer.SGD(learning_rate=base_lr, momentum=base_mom, wd=base_wd, rescale_grad=1)

  #create initializer
  if args.network[0]=='r' or args.network[0]=='y':
    initializer = mx.init.Xavier(rnd_type='gaussian', factor_type="out", magnitude=2) #resnet style
  elif args.network[0]=='i' or args.network[0]=='x':
    initializer = mx.init.Xavier(rnd_type='gaussian', factor_type="in", magnitude=2) #inception
  else:
    initializer = mx.init.Xavier(rnd_type='uniform', factor_type="in", magnitude=2)
  #train_dataiter = mx.io.PrefetchingIter(train_dataiter)
  
  #create callback for speed metrics
  som = 20
  _cb = mx.callback.Speedometer(args.per_batch_size, som)
  
  global_step = [0]
  save_step = [0]

  #learning rate schedule
  if len(args.lr_steps)==0:
    lr_steps = [40000, 60000, 80000]
    if args.loss_type>=1 and args.loss_type<=7:
      lr_steps = [100000, 140000, 160000]
    p = 512.0/args.per_batch_size
    for l in xrange(len(lr_steps)):
      lr_steps[l] = int(lr_steps[l]*p)
  else:
    lr_steps = [int(x) for x in args.lr_steps.split(',')]
  
  print('lr_steps', lr_steps)

  #create batch callback
  def _batch_callback(param):
    #global global_step
    global_step[0]+=1
    mbatch = global_step[0]
    for _lr in lr_steps:
      if mbatch==args.beta_freeze+_lr:
        opt.lr *= 0.1
        print('lr change to', opt.lr)
        break
    _cb(param)
    
    if mbatch%1000==0:
      print('lr-batch-epoch:',opt.lr,param.nbatch,param.epoch)

  #evaluate model
  def eval_model(epoch):
    args_dict = edict()
    args_dict.image_size = (112,112)
    args_dict.docselfie_model = args.prefix + "," + str(epoch)
    args_dict.selfie_lst = args.selfie_lst
    args_dict.selfie_lst = args.doc_lst
    extract_features(args_dict)

  
  model.fit(train_dataiter,
        begin_epoch        = begin_epoch,
        num_epoch          = args.end_epoch,
        eval_data          = None,
        eval_metric        = eval_metrics,
        kvstore            = 'device',
        optimizer          = opt,
        #optimizer_params   = optimizer_params,
        initializer        = initializer,
        arg_params         = all_args,
        aux_params         = all_aux,
        allow_missing      = True,
        batch_end_callback = _batch_callback,
        epoch_end_callback = mx.callback.do_checkpoint(args.prefix,period=args.save_epoch) )
  

def parse_args():
  parser = argparse.ArgumentParser(description='Train face network')
  # general
  parser.add_argument('--data-dir', default='', help='training set directory')
  parser.add_argument('--prefix', default='../model/model', help='directory to save model.')
  parser.add_argument('--pretrained', default='', help='pretrained model to load')
  parser.add_argument('--ckpt', type=int, default=1, help='checkpoint saving option. 0: discard saving. 1: save when necessary. 2: always save')
  parser.add_argument('--loss-type', type=int, default=4, help='loss type')
  parser.add_argument('--verbose', type=int, default=2000, help='do verification testing and model saving every verbose batches')
  parser.add_argument('--max-steps', type=int, default=0, help='max training batches')
  parser.add_argument('--end-epoch', type=int, default=100000, help='training epoch size.')
  parser.add_argument('--network', default='r50', help='specify network')
  parser.add_argument('--image-size', default='112,112', help='specify input image height and width')
  parser.add_argument('--version-se', type=int, default=0, help='whether to use se in network')
  parser.add_argument('--version-input', type=int, default=1, help='network input config')
  parser.add_argument('--version-output', type=str, default='E', help='network embedding output config')
  parser.add_argument('--version-unit', type=int, default=3, help='resnet unit config')
  parser.add_argument('--version-multiplier', type=float, default=1.0, help='filters multiplier')
  parser.add_argument('--version-act', type=str, default='prelu', help='network activation config')
  parser.add_argument('--use-deformable', type=int, default=0, help='use deformable cnn in network')
  parser.add_argument('--lr', type=float, default=0.1, help='start learning rate')
  parser.add_argument('--lr-steps', type=str, default='', help='steps of lr changing')
  parser.add_argument('--wd', type=float, default=0.0005, help='weight decay')
  parser.add_argument('--fc7-wd-mult', type=float, default=1.0, help='weight decay mult for fc7')
  parser.add_argument('--fc7-lr-mult', type=float, default=1.0, help='lr mult for fc7')
  parser.add_argument("--fc7-no-bias", default=False, action="store_true" , help="fc7 no bias flag")
  parser.add_argument('--bn-mom', type=float, default=0.9, help='bn mom')
  parser.add_argument('--mom', type=float, default=0.9, help='momentum')
  parser.add_argument('--emb-size', type=int, default=512, help='embedding length')
  parser.add_argument('--per-batch-size', type=int, default=128, help='batch size in each context')
  parser.add_argument('--margin-m', type=float, default=0.5, help='margin for loss')
  parser.add_argument('--margin-s', type=float, default=64.0, help='scale for feature')
  parser.add_argument('--margin-a', type=float, default=1.0, help='')
  parser.add_argument('--margin-b', type=float, default=0.0, help='')
  parser.add_argument('--easy-margin', type=int, default=0, help='')
  parser.add_argument('--margin', type=int, default=4, help='margin for sphere')
  parser.add_argument('--beta', type=float, default=1000., help='param for sphere')
  parser.add_argument('--beta-min', type=float, default=5., help='param for sphere')
  parser.add_argument('--beta-freeze', type=int, default=0, help='param for sphere')
  parser.add_argument('--gamma', type=float, default=0.12, help='param for sphere')
  parser.add_argument('--power', type=float, default=1.0, help='param for sphere')
  parser.add_argument('--scale', type=float, default=0.9993, help='param for sphere')
  parser.add_argument('--rand-mirror', type=int, default=1, help='if do random mirror in training')
  parser.add_argument('--cutoff', type=int, default=0, help='cut off aug')
  parser.add_argument('--color', type=int, default=0, help='color jittering aug')
  parser.add_argument('--images-filter', type=int, default=0, help='minimum images per identity filter')
  parser.add_argument('--target', type=str, default='lfw,cfp_fp,agedb_30', help='verification targets')
  parser.add_argument('--ce-loss', default=False, action='store_true', help='if output ce loss')
  parser.add_argument('--save-epoch', type=int,default=2,help='save checkpoint after every n epochs')
  parser.add_argument('--eval-epoch', type=int,default=2,help='eval checkpoint after every n epochs')
  parser.add_argument('--doc-test', type=str,default='',help='eval lst file for doc')
  parser.add_argument('--selfie-test', type=str,default='',help='selfie lst file for doc')
  parser.add_argument('--num-shared-stages', type=int,default=0,help='Number of resnet stages to share')
  args = parser.parse_args()
  return args

def main():
    #time.sleep(3600*6.5)
    global args
    args = parse_args()
    main_net(args)

if __name__ == '__main__':
    main()